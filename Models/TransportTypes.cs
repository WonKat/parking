namespace Parking.Models
{
    public enum TransportTypes
    {
        Car=1,
        Truck,
        Bus,
        Motorcycle
    }
}