namespace Parking.Models
{
    public  class Transport
    {
        public int Id { get; set; }
        public string DriverName { get; set; }
        public TransportTypes Type { get;}
        public double Balance { get; set; }

        public Transport(TransportTypes type)
        {
            Type = type;
        }

        public override string ToString()
        {
            return $"TransportId:{Id} Driver name: {DriverName} " +
                   $"Balance : {Balance} Type : {Type}";
        }
    }
}