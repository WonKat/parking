using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Parking.Abstractions;
using Parking.Exceptions;
using Parking.Models;


namespace Parking.Implementation
{
    public class Parking:IParking
    {
        private static object lockTransactions = new object();
        private static object lockTransportList = new object();
        public double Balance { get; set; }
        public List<Transport> ListOfTransport { get; set; }
        public Dictionary<TransportTypes, double> PriceList { get; set; }
        public List<Transaction> Transactions { get; set; }
        public int MaxParkingSpaciousness { get; set; }
        public double PeriodForPaymentsInSeconds { get; set; }
        public double TaxCoefficient { get; set; }

        public Parking()
        {
            ListOfTransport = new List<Transport>();
            Transactions = new List<Transaction>();
        }
        public void AddNewTransport(Transport transport)
        {
            lock (lockTransportList)
            {
                if (MaxParkingSpaciousness > ListOfTransport.Count &&
                    !ListOfTransport.Any(transports=> transports.Id == transport.Id))
                    ListOfTransport.Add(transport);
                else throw new FullParkingException("Parking is full remove any transport to enable adding new one");
            }
        }

        public List<Transport> GetAllAvailableTransport()
        {
            lock (lockTransportList)
            {
                return ListOfTransport;
            }
        }

        public List<Transaction> GetAllTransactionsForLastMinute()
        {
            lock (lockTransactions)
            {
                return Transactions;
            }
        }

        public List<string> GetAllTransactionsHistory()
        {
            lock (lockTransactions)
            {
                if(File.Exists("./Transactions.log"))
                { 
                    using(var streamReader = new StreamReader("./Transactions.log"))
                    {
                        return streamReader.ReadToEnd()
                            .Split('\n')
                            .ToList();
                    }
                }
                else
                {
                    throw  new FileNotFoundException("Transactions.log file not found");
                }
            }
        }

        public Tuple<int, int> GetInforationAboutFreeOccupiedPlaces()
        {
            lock (lockTransportList)
            {
                var countOccupiedPlaces = ListOfTransport.Count;
                return new Tuple<int, int>(countOccupiedPlaces,MaxParkingSpaciousness - countOccupiedPlaces);
            }
        }

        public double GetSalaryForLastMinute()
        {
            lock (lockTransactions)
            {
                return Transactions.Sum(transaction => transaction.Payment);
            }
        }

        public void GetTaxesFromTransport(object obj)
        {
            lock (lockTransportList)
            {
                foreach (var transport in ListOfTransport)
                {
                    var tax = PriceList.FirstOrDefault(trans => trans.Key == transport.Type).Value;
                    var IsTaxLowerThenTransportBalance = tax > transport.Balance  ;
                    if (IsTaxLowerThenTransportBalance)
                    {
                        tax *= TaxCoefficient;
                        transport.Balance -= tax;
                    }
                    else
                    {
                        transport.Balance -= tax;
                        Balance += tax;
                        AddNewTransaction(tax, transport);
                    }
                }
            }
        }

        public bool IsParkingEmpty()
        {
            lock (lockTransportList)
            {
                return !ListOfTransport.Any();
            }
        }

        public bool IsTransportWithSpecificIdOlreadyExist(int Id)
        {
            lock (lockTransportList)
            {
                return ListOfTransport.Any(transport => transport.Id == Id);
            }
        }

        public void RemoveExistTransport(int transportId)
        {
            lock (lockTransportList)
            {
                ListOfTransport.Remove(ListOfTransport.First(transport=>transport.Id == transportId));
            }
        }

        public void ReplenishBalanceForExistTransport(int transportId, int sum)
        {
            lock (lockTransportList)
            {
                var targetTransport = ListOfTransport.First(transport => transport.Id == transportId);
                if (targetTransport.Balance < 0)
                {
                    var debt = Math.Abs(targetTransport.Balance);
                    AddNewTransaction(debt,targetTransport);
                    Balance += debt;
                }
                targetTransport.Balance += sum;
            }
        }

        private void AddNewTransaction(double payment, Transport transport)
        {
            lock (lockTransactions)
            {
                Transactions.Add(new Transaction()
                {
                    Payment = payment,
                    Time = DateTime.Now,
                    Transport = transport
                });
            }
        }
        public void WriteTransactionsToFileAndClearCollection(object obj)
        {
            lock (lockTransactions)
            {
                if (!Transactions.Any()) return;
                 using (var streamWriter = new StreamWriter("./Transactions.log",true))
                 {
                    foreach (var transaction in Transactions)
                    {
                        streamWriter.WriteLine(transaction.ToString());
                    }
                 }
                Transactions.Clear();
            }
        }
    }
}