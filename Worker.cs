﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Parking.Abstractions;
using Parking.Exceptions;
using Parking.Models;

namespace Parking
{
    class Worker
    {
        private readonly IParking _parking;
        public Worker(IParking parking)
        {
            _parking = parking;
        }

        public void StartParking()
        {
            Console.CancelKeyPress += Console_CancelKeyPress;
            SetConfiguration();
            SetParking();
            SetTimers();
            UserInteractionWithMenu();
        }

        private void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            const string path = "./Transactions.log";
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        public void SetTimers()
        {
            new Timer(_parking.WriteTransactionsToFileAndClearCollection,
                null, TimeSpan.Zero, TimeSpan.FromSeconds(60));
            new Timer(_parking.GetTaxesFromTransport,
                null, TimeSpan.Zero, TimeSpan.FromSeconds(Configuration.PeriodForPaymentsInSeconds));
        }
        public void UserInteractionWithMenu()
        {
            bool exitFromProgramm = false;
            while (!exitFromProgramm)
            {

                Console.WriteLine("\t\tParking menu");
                Console.WriteLine("1) Show current parking balance");
                Console.WriteLine("2) Show number of free / occupied parking spaces");
                Console.WriteLine("3) Show transactions for last minute");
                Console.WriteLine("4) Show transaction history");
                Console.WriteLine("5) Show list of available transport");
                Console.WriteLine("6) Add new transport");
                Console.WriteLine("7) Remove exist transport");
                Console.WriteLine("8) Replenish balance for specific transport");
                Console.WriteLine("0) Exit");
                Console.WriteLine("\n");
                Console.Write("Your input: ");

                var rightInput = int.TryParse(Console.ReadLine(), out int input);
                switch (input)
                {
                    case 1:
                    {
                        Console.WriteLine($"Current parking balance is : {_parking.Balance}");
                        break;
                    }
                    case 2:
                    {
                        var freeOccuplied = _parking.GetInforationAboutFreeOccupiedPlaces();
                        Console.WriteLine($"{freeOccuplied.Item1} places are free and {freeOccuplied.Item2} places are occupied");
                        break;
                    }
                    case 3:
                    {                        
                        foreach (var transaction in _parking.GetAllTransactionsForLastMinute())
                        {
                            Console.WriteLine(transaction.ToString());
                        }
                        break;
                    }
                    case 4:
                    {
                        try
                        {
                            foreach (var transaction in _parking.GetAllTransactionsHistory())
                            {
                                Console.WriteLine(transaction);
                            }
                        }
                        catch (FileNotFoundException)
                        {
                            Console.WriteLine("Journal with transactions is empty");                            
                        }
                            
                        break;
                    }
                    case 5:
                    {
                        foreach (var transport in _parking.GetAllAvailableTransport())
                        {
                            Console.WriteLine(transport.ToString());
                        }

                        break;
                    }
                    case 6:
                    {
                        UserInputToCreatenewTransport();
                        break;
                    }
                    case 7:
                    {
                        if(_parking.IsParkingEmpty())
                        {
                            Console.WriteLine("Parking is empty(");
                            break;
                        }
                        else
                        {
                            try
                            {
                                _parking.RemoveExistTransport(
                                    ValidateIntInput("Input transport Id which you wanna remove"));
                            }
                           catch (InvalidOperationException )
                            {
                                Console.WriteLine("Parking haven't transport with same id");                                
                            }
                        }
                        break;
                    }
                    case 8:
                    {
                        if (_parking.IsParkingEmpty())
                        {
                            Console.WriteLine("Parking is empty(");
                            break;
                        }
                        else
                        {
                            try
                            {
                                _parking.ReplenishBalanceForExistTransport(
                                    ValidateIntInput("Input transport Id which u wanna replenish"),
                                    ValidateIntInput("Input sum for replenish "));
                            }
                            catch (InvalidOperationException)
                            {
                                Console.WriteLine("Parking haven't transport with same id");
                            }

                            break;
                        }
                    }
                    case 0:
                    {
                        exitFromProgramm = true;
                        Console.WriteLine("Good bye");
                        break;
                    }
                    default:
                    {
                        Console.WriteLine("Invalid input please input number 1-8 or 0");
                        break;
                    }
                }
                ClearConsole();
            }
        }

        private void UserInputToCreatenewTransport()
        {
            Console.WriteLine("So we create new transport");
            var type = ValidateTypeInput(
                "Select what type transport will be ([1] - Car, [2] - Truck, [3] - Bus, [4] - Motorcicle)");      
            var transport = new Transport(type)
            {
                Id= ValidateIdInput("Input unique Id for new transport"),
                Balance = ValidateDoubleInput("Input start balance")
            };
            Console.WriteLine("Input driver name");
            transport.DriverName = Console.ReadLine();            
            try
            {
                _parking.AddNewTransport(transport);
            }
            catch (FullParkingException e)
            {
                Console.WriteLine(e.Message);
            }
        }
       
        private void SetParking()
        {
            _parking.Balance = Configuration.StartParkingBalance;
            _parking.MaxParkingSpaciousness = Configuration.MaxParkingSpaciousness;
            _parking.PeriodForPaymentsInSeconds = Configuration.PeriodForPaymentsInSeconds;
            _parking.TaxCoefficient = Configuration.TaxCoefficient;
            _parking.PriceList = new Dictionary<TransportTypes, double>()
            {
                { TransportTypes.Bus,3.5},
                { TransportTypes.Car,2},
                { TransportTypes.Truck,5},
                { TransportTypes.Motorcycle,1},
            };
           ClearConsole();
        }
        private void SetConfiguration()
        {
            Console.WriteLine("Welcome to the parking");
            Console.WriteLine("Start with default values " +
                              "(input [no] if u don't wanna use default values)");
            var overrideDefaultValues = Console.ReadLine().ToLower().Equals("no");
            if (overrideDefaultValues)
            {
                Console.WriteLine("Ok, seems like u wanna override default values");
                Configuration.StartParkingBalance = ValidateDoubleInput("Input start parking money balance");
                Configuration.MaxParkingSpaciousness = ValidateIntInput("Input number of max available parking places");
                Configuration.PeriodForPaymentsInSeconds = ValidateDoubleInput("Input the payment period (in seconds)");
                Configuration.TaxCoefficient = ValidateDoubleInput("Input tax coefficient for parking");
            }
        }

        private int ValidateIdInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int input) &&
                    !_parking.IsTransportWithSpecificIdOlreadyExist(input))
                {
                    return input;
                }
                Console.WriteLine("Invalid input you must input numeric value >= 1 or transport with same Id already exist, try one more time");
            }
        }
        private TransportTypes ValidateTypeInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int input) &&
                    input >= 1 && input <=4)
                {
                    switch (input)
                    {
                        case 1:
                            return TransportTypes.Car;
                        case 2:
                            return TransportTypes.Truck;
                        case 3:
                            return TransportTypes.Bus;
                        case 4:
                            return TransportTypes.Motorcycle;
                        default:
                            break;                        
                    }
                }
                Console.WriteLine("Invalid input you must input integer value >= 1 and <= 4, try one more time");
            }
        }
        private int ValidateIntInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);

            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int input) && 
                    input >= 1)
                {
                    return input;
                }
                Console.WriteLine("Invalid input you must input integer value >= 1, try one more time");
            }
        }
        private double ValidateDoubleInput(string parameterDescription)
        {
            Console.WriteLine(parameterDescription);
            while (true)
            {
                if (double.TryParse(Console.ReadLine(), out double input) &&
                    input>=1)
                {
                    return input;
                }
                Console.WriteLine("Invalid input you must input numeric value >= 1, try one more time");
            }
        }

        private void ClearConsole()
        {
            Console.Write("Press any key to continue ...");
            Console.ReadKey(true);
            Console.Clear();
        }
    }
}
