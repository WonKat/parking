using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Parking.Models;

namespace Parking.Abstractions
{
    public interface IParking
    {
        double Balance { get; set; }
        int MaxParkingSpaciousness { get; set; }
        double PeriodForPaymentsInSeconds { get; set; }
        double TaxCoefficient { get; set; }
        List<Transport> ListOfTransport { get; set;}
        Dictionary<TransportTypes, double> PriceList { get; set; }
        List<Transaction> Transactions { get; set; }
        Tuple<int, int> GetInforationAboutFreeOccupiedPlaces();
        List<Transaction> GetAllTransactionsForLastMinute();
        List<string> GetAllTransactionsHistory();
        List<Transport> GetAllAvailableTransport();
        void AddNewTransport(Transport transport);
        void RemoveExistTransport(int transportId);
        void ReplenishBalanceForExistTransport(int transportId, int sum);
        void WriteTransactionsToFileAndClearCollection(object obj);
        void GetTaxesFromTransport(object obj);
        double GetSalaryForLastMinute();
        bool IsTransportWithSpecificIdOlreadyExist(int Id);
        bool IsParkingEmpty();
    }
}